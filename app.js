const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
// require('dotenv').config();
const config = require('dotenv');
config.config();
const mongoose = require('mongoose');
const passport = require('passport');
const helmet = require("helmet");
const cors = require("cors");

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const categoryRouter = require('./routes/category');
const blogRouter = require('./routes/blog');

//middlewares
const errorHandler = require('./middlewares/errorHandler');
// const passportJWT = require('./middlewares/passportJWT');

const app = express();

app.use(cors());
app.use(helmet());

app.use(logger('dev'));
app.use(express.json({
    limit: "50mb"
}));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//connect to mongodb server
mongoose.connect(process.env.MONGO_URI).then(() => {
    console.log("connect to mongodb success!");
}).catch((err) => {
    console.log(err.codeName);
});

//init passportjs
passport.initialize();

app.use('/', indexRouter); // localhost:4000
app.use('/api/users', usersRouter); // localhost:4000/api/users
app.use('/api/category', categoryRouter); // localhost:4000/api/category
app.use('/api/blog', blogRouter); // localhost:4000/api/blog

app.use(errorHandler);

module.exports = app;
