const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { Schema } = mongoose;

const blogSchema = new Schema(
  {
    topic: String,
    photo: { type: String, default: "nopic.png" },
    user: { type: Schema.Types.ObjectId, ref: "User" }
  },
  {
    collection: "blog",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
    toJSON: { virtuals: true }
  }
);

blogSchema.virtual("photo_url").get(function() {
  return process.env.UPLOAD_URL + this.photo;
});

blogSchema.plugin(mongoosePaginate);

const Blog = mongoose.model("Blog", blogSchema);

module.exports = Blog;