const argon2 = require("argon2");
const mongoose = require('mongoose');
const { Schema } = mongoose;

const userSchema = new Schema(
  {
    name: { type: String, trim: true, required: true },
    email: { type: String, trim: true, required: true, unique: true, index: true },
    password: { type: String, trim: true, required: true, minlength: 3 },
    role: { type: String, default: 'member' }
  },
  {
    collection: "user",
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" }
  }
);

userSchema.methods.hashPassword = async function(password) {
    const hash = await argon2.hash(password);
    return hash;
}

userSchema.methods.verifyPassword = async function(password) {
    const isValid = await argon2.verify(this.password, password);
    return isValid; // true
}

const User = mongoose.model("User", userSchema);

module.exports = User;