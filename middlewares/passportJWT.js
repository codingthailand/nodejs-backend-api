const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy,
    ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');

const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
opts.secretOrKey = process.env.JWT_SECRET;
// opts.issuer = 'accounts.examplesoft.com';
// opts.audience = 'yoursite.net';

passport.use(new JwtStrategy(opts, async function(jwt_payload, done) {
    try {
        const user = await User.findById(jwt_payload.id); // id from payload
        if (!user) {
            return done(new Error("ไม่พบผู้ใช้นี้ในระบบ"), null);
        }
        return done(null, user); // req.user
    } catch (error) {
        done(error);
    }
}));

// check isLogin
module.exports.isLogin = passport.authenticate('jwt', { session: false });