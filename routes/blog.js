const express = require("express");
const { index, create } = require("../controllers/blog-controller");
const router = express.Router();

const passportJWT = require("../middlewares/passportJWT");

/* localhost:4000/api/blog/ */
router.get("/", index );

/* localhost:4000/api/blog/ */
router.post("/", passportJWT.isLogin , create );

module.exports = router;
