const express = require("express");
const { body } = require("express-validator");
const { index, register, login, getProfile } = require("../controllers/users-controller");
const router = express.Router();
const passportJWT = require("../middlewares/passportJWT");
const checkAdmin = require("../middlewares/checkAdmin");

// get user's profile
/* localhost:4000/api/users/profile */
router.get("/profile", [passportJWT.isLogin], getProfile );

/* localhost:4000/api/users/ */
router.get("/", [passportJWT.isLogin, checkAdmin.isAdmin], index );

/* localhost:4000/api/users/register */
router.post("/register", [
    body('name').not().isEmpty().withMessage("ชื่อสกุล ห้ามว่าง"),
    body('email').not().isEmpty().withMessage("อีเมล์ ห้ามว่าง").isEmail().withMessage("รูปแบบอีเมล์ไม่ถูกต้อง"),
    body('password').not().isEmpty().withMessage("รหัสผ่าน ห้ามว่าง").isLength({min: 3}).withMessage("รหัสผ่านต้องอย่างน้อย 3 ตัวอักษร"),
] , register );

/* localhost:4000/api/users/login */
router.post("/login", [
    body('email').not().isEmpty().withMessage("อีเมล์ ห้ามว่าง").isEmail().withMessage("รูปแบบอีเมล์ไม่ถูกต้อง"),
    body('password').not().isEmpty().withMessage("รหัสผ่าน ห้ามว่าง").isLength({min: 3}).withMessage("รหัสผ่านต้องอย่างน้อย 3 ตัวอักษร"),
] , login );

module.exports = router;
