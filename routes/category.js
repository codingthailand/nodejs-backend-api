const express = require("express");
const { index, show, create, update, destroy, search } = require("../controllers/category-controller");
const router = express.Router();

/* localhost:4000/api/category/search?name=กีฬา */
router.get("/search", search );

/* localhost:4000/api/category/ */
router.get("/", index );

/* localhost:4000/api/category/6 */
router.get("/:id", show );

/* localhost:4000/api/category/ */
router.post("/", create );

/* localhost:4000/api/category/6 */
router.put("/:id", update );

/* localhost:4000/api/category/6 */
router.delete("/:id", destroy );

module.exports = router;
