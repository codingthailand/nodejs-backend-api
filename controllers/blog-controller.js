const Blog = require("../models/blog");
const uploadImageToDisk = require("../services/upload");

// exports.index = async (req, res, next) => {
//     const blog = await Blog.find().populate('user', 'name email').sort({ _id: -1});

//     return res.status(200).json({
//         data: blog
//     });
// }

// Pagination
exports.index = async (req, res, next) => {
    // http://localhost:4000/api/blog?page=1&pageSize=3
    const { page, pageSize } = req.query;

    const options = {
        select: 'topic photo photo_url created_at',
        sort: { _id: -1 },
        // populate: 'user',
        populate: [{
            path: 'user',
            select: 'name email'
        }],
        customLabels: { docs: 'data', totalDocs: 'totalRecord' },
        page: Number(page),
        limit: Number(pageSize)
    };

    const blog = await Blog.paginate({}, options);

    return res.status(200).json(blog);
}

exports.create = async (req, res, next) => {
    const { topic, photo } = req.body;

    const blog = new Blog({
        topic: topic,
        photo: await uploadImageToDisk(photo),
        user: req.user._id
    });

    await blog.save();

    return res.status(201).json({
        message: "เพิ่มข้อมูล Blog สำเร็จ"
    });
}