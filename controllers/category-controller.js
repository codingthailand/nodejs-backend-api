const Category = require("../models/category");

exports.index = async function (req, res, next) {
    // const category = await Category.find().sort("-_id");
    const category = await Category.find().select("name created_at").sort("-_id");
    return res.status(200).json({ data: category });
}

exports.show = async function (req, res, next) {
    try {
        const { id } = req.params;
        const category = await Category.findById(id);
        if (!category) {
           const error = new Error("ไม่พบข้อมูลนี้ในระบบ");
           error.statusCode = 404;
           throw error;     
        }
        return res.status(200).json(category);
    } catch (error) {
        next(error);
    }
}

exports.create = async function (req, res, next) {
    const { name } = req.body;

    const category = new Category({ name: name });
    const result = await category.save();

    return res.status(201).json({ 
        message: "บันทึกข้อมูลสำเร็จ",
        data: result
    });
}

exports.update = async function (req, res, next) {
    const { id } = req.params;
    const { name } = req.body;

    const category = await Category.updateOne({ _id: id }, {
        name: name
    });

    return res.status(200).json({ 
        message: "แก้ไขข้อมูลสำเร็จ",
        data: category
    });
}

exports.destroy = async function (req, res, next) {
    const { id } = req.params;
    const category = await Category.deleteOne({ _id: id});
    return res.status(200).json({ 
        message: "ลบข้อมูลสำเร็จ",
        data: category
    });
}

exports.search = async function (req, res, next) {
    const { name } = req.query;
    const category = await Category.find({ name: { $regex: ".*" + name + ".*", $options: 'i' } });

    return res.status(200).json({ data: category });
}

