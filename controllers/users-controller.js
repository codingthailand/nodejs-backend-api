const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

const User = require("../models/user");

exports.index = function (req, res, next) {
  return res.status(200).json({ message: "Hello Users Index!" });
};

exports.register = async function (req, res, next) {
  try {
    const { name, email, password } = req.body;

    //validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("ข้อมูลที่ส่งมาไม่ถูกต้อง");
      error.statusCode = 422;
      error.validation = errors.array();
      throw error;
    }

    //check email ซ้ำ
    const existEmail = await User.findOne({ email: email });
    if (existEmail) {
      const error = new Error("อีเมล์ซ้ำ มีผู้ใช้งานแล้ว");
      error.statusCode = 400;
      throw error;
    }

    const user = new User();
    user.name = name;
    user.email = email;
    user.password = await user.hashPassword(password);
    await user.save();

    return res.status(200).json({ message: "ลงทะเบียนสำเร็จ" });

  } catch (error) {
    next(error);
  }
};

// Login
exports.login = async function (req, res, next) {
  try {
    const { email, password } = req.body;

    //validation
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const error = new Error("ข้อมูลที่ส่งมาไม่ถูกต้อง");
      error.statusCode = 422;
      error.validation = errors.array();
      throw error;
    }

    //check ว่ามีอีเมล์นี้ในระบบหรือไม่
    const user = await User.findOne({ email: email });
    if (!user) {
      const error = new Error("ไม่พบผู้ใช้นี้ในระบบ");
      error.statusCode = 404;
      throw error;
    }

    //เปรียบเทียบรหัสผ่านที่ส่งมากับฐานข้อมูลว่าตรงกันหรือไม่
    const isValid = await user.verifyPassword(password);
    if (!isValid) {
      const error = new Error("รหัสผ่านไม่ถูกต้อง");
      error.statusCode = 401;
      throw error;
    }

    //สร้าง token
    const token = jwt.sign(
      {
        id: user._id,
        role: user.role,
      },
      process.env.JWT_SECRET,
      {
        expiresIn: "5d",
      }
    );
    //decode วันหมดอายุ
    const tokenDecode = jwt.decode(token);

    return res.status(201).json({
      token_type: "Bearer",
      access_token: token,
      expires_in: tokenDecode.exp,
    });

  } catch (error) {
    next(error);
  }
};

//get user's profile
exports.getProfile = function(req, res, next) {
   // req.user
   const { _id, name, email, role } = req.user;

   return res.status(200).json({
      id: _id,
      name: name,
      email: email,
      role: role
   });
}
